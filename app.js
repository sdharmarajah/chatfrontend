// importing modules
const express = require('express');
const subdomain = require('express-subdomain');
const bodyparser = require('body-parser');
const cors = require('cors');
const path = require('path');
var request = require('request');
const fs = require('fs');

var engines = require('consolidate');

const router = express.Router();

var app = express();

app.use(express.static(__dirname + '/views/'));

app.engine('html', engines.mustache);
app.set('view engine', 'html');
// adding middleware - cors
app.use(cors());

// body-parser
app.use(bodyparser.urlencoded({ extended: false}));
app.use(bodyparser.json());

router.get('/', function(req, res) {
	res.render("index");
});

router.post('/getquery', function(req, res) {
	request.post(
	    'http://13.250.170.79/query',
	    { json: req.body },
	    function (error, response, body) {
	    	var str='';
	        if (!error && response.statusCode == 200) {
	        	body.forEach(function(element){
	        		str += '<br>'
	        		str += element.result.fulfillment.speech;
	        		//str += '\n';
	        	});
	        	res.send(str);
	        }
	    }
	);

});

app.use('/', router);

app.listen(3000);