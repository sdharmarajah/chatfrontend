var ts = Math.round((new Date()).getTime() / 1000);

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

function insertChat(who, text, time = 0){
    var control = "";
    var date = formatAMPM(new Date());
    
    if (who == "me"){
        control = '<div class="row msg_container base_sent" style="margin-right:20px;margin-top:20px">' +
                        '<div class="col-md-11 col-xs-11" align="right">' +
                            '<div class="messages msg_sent" style="margin-top:20px">' +
                                text  + '<br>' + 
                                '<time datetime="2009-11-13T20:00">' + date + '</time>' +
                            '</div>' +
                        '</div>' +
                        '<div class="col-md-1 col-xs-1 avatar" style="margin-top:-5px">' +
                            '<img src="./assets/1.png" class=" img-responsive ">' +
                        '</div>' +
                    '</div>';

    }else{
        control = '<div class="row msg_container base_receive" style="margin-left:20px;margin-top:20px">' +
                        '<div class="col-md-1 col-xs-1 avatar" style="margin-top:-5px">' +
                            '<img src="./assets/2.png" class=" img-responsive ">' +
                        '</div>' +
                        '<div class="col-md-10 col-xs-10">' +
                            '<div class="messages msg_receive" style="margin-top:-20px">' +
                                text  + '<br>'  +
                                '<time datetime="2009-11-13T20:00">' + date + '</time>' +
                            '</div>' +
                        '</div>' +
                    '</div>';
    }
    setTimeout(
        function(){                        
            $("#msgbody").append(control);
            $('#msgbody').scrollTop($('#msgbody')[0].scrollHeight);
        }, time);
    
}

function resetChat(){
    $("#msgbody").empty();
    // insertChat("me","Hello!");
    // insertChat("server","Nice to meet you! How may I help you?");
}

$("#chat-input").on("keyup", function(e){
    if (e.which == 13){
        onMessageSend();
    }
});

function onMessageSend()
{
    var text = $("#btn-input").val();
    if (text !== ""){
        insertChat("me", text);              
        $("#btn-input").val('');
    }
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          insertChat("server", xhttp.responseText);
        }
    };
    xhttp.open("POST", "/getquery", true);
    xhttp.setRequestHeader('Content-type','application/json; charset=utf-8');
    xhttp.send(JSON.stringify({ 'initialize': 0, 'lang': 'en', 'query':text , 'sessionId':JSON.stringify(ts) , 'timezone':JSON.stringify(Intl.DateTimeFormat().resolvedOptions().timeZone) }));
}


resetChat();

var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      insertChat("server", xhttp.responseText);
    }
};
xhttp.open("POST", "/getquery", true);
xhttp.setRequestHeader('Content-type','application/json; charset=utf-8');
xhttp.send(JSON.stringify({ 'initialize': 0, 'lang': 'en', 'query':'cough and fever' , 'sessionId':JSON.stringify(ts) , 'timezone':JSON.stringify(Intl.DateTimeFormat().resolvedOptions().timeZone) }));
